#!/usr/bin/env bash

export LATEST_VERSION=$(curl --silent "https://api.github.com/repos/rust-lang/rust/releases/latest" | jq -r '.tag_name')
export RELEASE_BODY=$(curl --silent "https://api.github.com/repos/rust-lang/rust/releases/latest" | jq -r '.body')
export GIT_TAGS=$(git tag)

echo "LATEST_VERSION=$LATEST_VERSION" >> version.env
echo "$RELEASE_BODY" >> release_body.txt

if [[ "$GIT_TAGS" =~ "$LATEST_VERSION" ]]; then
    cp ci/noop.yml ci.yml
else
    cp ci/build.yml ci.yml
fi
