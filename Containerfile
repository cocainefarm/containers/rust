ARG RUST_VERSION
ARG BASE_IMAGE=docker.io/rust:${RUST_VERSION}-alpine

FROM $BASE_IMAGE as mold-build

RUN apk add alpine-sdk cmake clang openssl-dev zlib-dev xxhash-dev

FROM docker.io/ubuntu:20.04 as mold-build
RUN ln -fs /usr/share/zoneinfo/Europe/Berlin /etc/localtime
RUN sed -i '/_apt/d' /etc/passwd && apt-get update && \
    TZ=Europe/London apt-get install -y tzdata && \
    apt-get install -y build-essential git clang lld cmake \
      libstdc++-10-dev libxxhash-dev zlib1g-dev libssl-dev && \
    rm -rf /var/lib/apt/lists/*

ENV LDFLAGS='-fuse-ld=lld -static'
ENV LDFLAGS="$LDFLAGS -Wl,-u,pthread_rwlock_rdlock"
ENV LDFLAGS="$LDFLAGS -Wl,-u,pthread_rwlock_unlock"
ENV LDFLAGS="$LDFLAGS -Wl,-u,pthread_rwlock_wrlock"

ARG MOLD_VERSION="v0.9.6"
RUN git clone --single-branch --branch "$MOLD_VERSION" https://github.com/rui314/mold.git
RUN make -C /mold -j$(nproc) EXTRA_LDFLAGS="$LDFLAGS"

FROM $BASE_IMAGE as builder

RUN apk add alpine-sdk openssl-dev perl
RUN cargo install cargo-tarpaulin --features vendored-openssl
RUN cargo install cargo-udeps
RUN cargo install cargo-chef
RUN cargo install cargo2junit

FROM $BASE_IMAGE
RUN apk add musl-dev

COPY --from=mold-build /mold/mold /usr/local/bin/mold
COPY --from=builder /usr/local/cargo/bin/cargo-tarpaulin /usr/local/cargo/bin/cargo-tarpaulin
COPY --from=builder /usr/local/cargo/bin/cargo-udeps /usr/local/cargo/bin/cargo-udeps
COPY --from=builder /usr/local/cargo/bin/cargo-chef /usr/local/cargo/bin/cargo-chef
COPY --from=builder /usr/local/cargo/bin/cargo2junit /usr/local/cargo/bin/cargo2junit

COPY config.toml /usr/local/cargo/config.toml
